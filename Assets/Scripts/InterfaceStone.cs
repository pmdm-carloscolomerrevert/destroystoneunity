﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class InterfaceStone : MonoBehaviour
{
    public Text textThrow;
    public Text textDestroyed;
    public Text textNivel;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        textThrow.text = "Number of Stones: " + GameManager.currentNumbersStonesThrown;
        textDestroyed.text = "Destroyed: " + GameManager.currentNumberDestroyedStones;
        textNivel.text = "Nivel: " + GameManager.currentNivel;
    }

   
}

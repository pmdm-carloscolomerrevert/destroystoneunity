﻿using UnityEngine;
using System.Collections;

public class MainLoop: MonoBehaviour
{

	private int NVL1_DESTROY = 5;
	private int NVL2_DESTROY = 10;
	private int NVL3_DESTROY = 15;
	public GameObject[] stones = new GameObject[4];
	public float torque = 5.0f;
	public float minAntiGravity = 20.0f, maxAntiGravity = 40.0f;
	public float minLateralForce = -15.0f, maxLateralForce = 15.0f;
	public float minTimeBetweenStones, maxTimeBetweenStones;
	public float minX = -30.0f, maxX = 30.0f;
	public float minZ = -5.0f, maxZ = 20.0f;
	
	private bool enableStones = true;
	private Rigidbody rigidbody;
	
	// Use this for initialization
	void Start () {
		StartCoroutine(ThrowStones());
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	IEnumerator ThrowStones()
	{
		// Initial delay
		yield return new WaitForSeconds(2.0f);
		
		while(enableStones) {

		
			if(GameManager.currentNumberDestroyedStones < NVL1_DESTROY ){
				minTimeBetweenStones = 3f;
				maxTimeBetweenStones = 3.5f;
				GameManager.currentNivel = 1;

			}
			else if (GameManager.currentNumberDestroyedStones < NVL2_DESTROY)
			{
				minTimeBetweenStones = 1.5f;
				maxTimeBetweenStones = 2f;
				GameManager.currentNivel = 2;

			}else if (GameManager.currentNumberDestroyedStones < NVL3_DESTROY) {

				minTimeBetweenStones = 0.5f;
				maxTimeBetweenStones = 1f;
				GameManager.currentNivel = 3;
			
			}

			GameObject stone = (GameObject) Instantiate(stones[Random.Range(0, stones.Length)]);
			stone.transform.position = new Vector3(Random.Range(minX, maxX), -30.0f, Random.Range(minZ, maxZ));
			stone.transform.rotation = Random.rotation;

			rigidbody = stone.GetComponent<Rigidbody>();
			
			rigidbody.AddTorque(Vector3.up * torque, ForceMode.Impulse);
			rigidbody.AddTorque(Vector3.right * torque, ForceMode.Impulse);
			rigidbody.AddTorque(Vector3.forward * torque, ForceMode.Impulse);
			
			rigidbody.AddForce(Vector3.up * Random.Range(minAntiGravity, maxAntiGravity), ForceMode.Impulse);
			rigidbody.AddForce(Vector3.right * Random.Range(minLateralForce, maxLateralForce), ForceMode.Impulse);

			if (!stone.tag.Equals("bomba"))
			{
				GameManager.currentNumbersStonesThrown++;
			}
			
			yield return new WaitForSeconds(Random.Range(minTimeBetweenStones, maxTimeBetweenStones));
			
		}
		
		
	}
}


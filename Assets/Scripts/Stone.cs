﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour
{

    public GameObject explosion;
    private const float yDie = -30.0f;
    private bool destroyed;
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < yDie)
        {
            Destroy(gameObject);
        }
        


    }


    private void OnMouseDown()
    {

        if (!gameObject.tag.Equals("bomba"))
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
            GameManager.currentNumberDestroyedStones++;
        }
        else
        {
            Application.LoadLevel("Final");
        }
        
          
       
    }
}
